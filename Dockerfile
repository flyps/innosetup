FROM debian:jessie

ENV DEBIAN_FRONTEND noninteractive

RUN dpkg --add-architecture i386
RUN sed -i "s/main/main contrib non-free/" etc/apt/sources.list
RUN apt-get update && apt-get install -y wine curl unrar unzip && rm -rf /var/lib/apt/lists/*

ENV INNOSETUP_VERSION 5.5.9
ENV INNOSETUP_DOWNLOAD_URL http://files.jrsoftware.org/is/5/innosetup-$INNOSETUP_VERSION-unicode.exe

RUN mkdir innosetup && \
    cd innosetup && \
    curl -fsSL -o innounp046.rar "http://downloads.sourceforge.net/project/innounp/innounp/innounp%200.46/innounp046.rar" && \
    unrar e innounp046.rar

RUN cd innosetup && \
    curl -fsSL -o is-unicode.exe $INNOSETUP_DOWNLOAD_URL && \
    wine "./innounp.exe" -e "is-unicode.exe"

RUN mkdir /src
WORKDIR /src

COPY iscc /usr/local/bin